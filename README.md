eComStyle.de::AutoLoad
==========================

#### Composer name
`ecs/autoload`


#### Shopversionen
`OXID eShop CE/PE 6`


#### Installation Produktiv-Umgebung
`composer require ecs/autoload --update-no-dev`


#### Installation Dev-Umgebung
`composer require ecs/autoload`
